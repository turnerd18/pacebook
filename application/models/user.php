<?php

class User extends CI_Model 
{
	function validate()
	{
		$this->db->where('email', $this->input->post('username'));
		$this->db->where('password', md5($this->input->post('password')));
		$result = $this->db->get('pb_Users');
		$user = $result->row();
		
		return $result->num_rows == 1 ? 
			array(
				'name' => $user->name, 
				'email' => $user->email,
				'photo' => $user->photo, 
				'phone' => $user->phone,
				'address' => $user->address,
				'is_staff' => $user->is_staff
			) : '';
	}
	
	function register()
	{
		// Confirm passwords are same
		$pw1 = $this->input->post('password');
		$pw2 = $this->input->post('password2');
		if ($pw1 != $pw2)
		{
			return '';
		}
		
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));
		$name = $this->input->post('name');
		$phone = $this->input->post('phone');
		$photo = 'http://www.gravatar.com/avatar/' . md5(strtolower(trim($email))) . '.jpg' . '?d=' . urlencode( base_url() . 'assets/images/nopic.jpg');
		$address = $this->input->post('address');
    $is_staff = false;
		
		$result = $this->db->simple_query("INSERT INTO pb_Users (email, password, name, phone, photo, address, is_staff) VALUES ('$email', '$password', '$name', '$phone', '$photo', '$address', '$is_staff')");
		
		return $result == true ? array('name' => $name, 'photo' => $photo, 'is_staff' => $is_staff, 'email' => $email, 'phone' => $phone, 'address' => $address) : '';
	}

	function update()
	{
		$post_data = $this->input->post();
		$user_data = $this->session->userdata;

		// If user changes email address, check if someone else already has the new address
		if ($user_data['email'] != $post_data['email'])
		{
			$new_email = $post_data['email'];
			$exists = $this->db->simple_query("SELECT email FROM pb_Users WHERE email='$new_email'");
			if ($exists == true)
			{
				return '';
			}
		}

		$cur_email = $user_data['email'];
		$email = $post_data['email'];
		$name = $post_data['name'];
		$phone = $post_data['phone'];
		$address = $post_data['address'];
		$password = md5($post_data['password']);
		
		$query = "UPDATE pb_Users SET email = '$email', name = '$name', phone = '$phone', address = '$address'";
		
		// Check if passwords matched
		if (!(empty($post_data['password']) || empty($post_data['password2'])))
		{
			if ($post_data['password'] != $post_data['password2'])
			{
				return '';
			}
			else
			{
				$query .= ", password = '$password'";
			}
		}

		$query .= " WHERE email = '$cur_email';";

		// Perform the updatea

		$result = $this->db->simple_query($query);
		if ($result == true)
		{
			$this->session->set_userdata('name', $this->input->post('name'));
			$this->session->set_userdata('email', $this->input->post('email')); 
			$this->session->set_userdata('address', $this->input->post( 'address' )); 
			$this->session->set_userdata('phone', $this->input->post( 'phone' )); 
			$this->session->set_userdata('photo', 'http://www.gravatar.com/avatar/' . md5(strtolower(trim($this->input->post( 'email' )))) . '.jpg' . '?d=' . urlencode( base_url() . 'assets/images/nopic.jpg'));

			return true;
		}
		else 
		{
			return '';
		}
	}
}
