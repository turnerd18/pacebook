<h2 class="text-center">Create A Network</h2>

<div style="padding: 20px 15px;">
	<?php echo form_open('home/createNetwork'); ?>
	<div class="row form-group">
		<label for="name" class="col-sm-offset-2 col-sm-2 control-label text-right">Network Name:</label>
		<div class="col-sm-4">
			<?php echo form_input( 
				array(
					'name' => 'name',
					'class' => 'form-control'
				)
			);?>
		</div>
	</div>
	<div class="row form-group">
		<label for="description" class="col-sm-offset-2 col-sm-2 control-label text-right">Description:</label>
		<div class="col-sm-4">
			<?php echo form_input(
				array(
					'name' => 'description',
					'class' => 'form-control'
				)
			);?>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-offset-4 col-sm-4">
			<input type="submit" name="submit" value="Submit" class="btn btn-info" />
		</div>
	</div>
	</form>
</div>
