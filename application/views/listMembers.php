<div class="row">
	<div class="col-md-offset-3 col-md-6 text-center">
		<h2>Available Friends</h2>
	</div>
</div>

<div class="row form-group">
	<div class="col-md-offset-3 col-md-6">
		<?php echo form_open('home/addFriend'); ?>
			<table class="table table-striped">
				<tr>
					<th></th>
					<th>Name</th>
					<th>Options</th>
				</tr>
				<?php for ($i = 0; $i < count($query_result); $i++) : ?>
					<tr>
						<td><img src="<?php echo $query_result[$i]->photo . '&s=40'; ?>" /></td>
						<td><?php echo $query_result[$i]->name; ?></td>
						<td>
							<?php echo form_hidden("friend_$i", $query_result[$i]->email); ?>
							<?php echo form_submit( 
								array( 
									'name' => "submit_$i", 
									'value' => 'Send Friend Request', 
									'class' => 'btn-sm btn-default'
								) 
							); ?>
						</td>
					</tr>
				<?php endfor; ?>
			</table>
		</form>
	</div>
</div>
