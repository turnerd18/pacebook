<h2 class="text-center">Update Your Information</h2>

<div style="padding: 20px 15px;">
	<?php echo form_open('home/update_profile'); ?>
	<div class="row form-group">
		<label for="email" class="col-sm-offset-2 col-sm-2 control-label text-right">Email Address:</label>
		<div class="col-sm-4">
			<?php echo form_input( 
				array(
					'name' => 'email',
					'value' => $this->session->userdata['email'],
					'class' => 'form-control'
				)
			);?>
		</div>
	</div>
	<div class="row form-group">
		<label for="password" class="col-sm-offset-2 col-sm-2 control-label text-right">Password:</label>
		<div class="col-sm-4">
			<?php echo form_password(
				array(
					'name' => 'password',
					'class' => 'form-control'
				)
			);?>
		</div>
	</div>
	<div class="row form-group">
		<label for="password2" class="col-sm-offset-2 col-sm-2 control-label text-right">Confirm Password:</label>
		<div class="col-sm-4">
			<?php echo form_password(
				array(
					'name' => 'password2',
					'class' => 'form-control'
				)
			);?>
		</div>
	</div>
	<div class="row form-group">
		<label for="name" class="col-sm-offset-2 col-sm-2 control-label text-right">Full Name:</label>
		<div class="col-sm-4">
			<?php echo form_input( 
				array(
					'name' => 'name',
					'value' => $this->session->userdata['name'],
					'class' => 'form-control'
				)
			);?>
		</div>
	</div>
	<div class="row form-group">
		<label for="phone" class="col-sm-offset-2 col-sm-2 control-label text-right">Phone Number:</label>
		<div class="col-sm-4">
			<?php echo form_input( 
				array(
					'name' => 'phone',
					'value' => $this->session->userdata['phone'],
					'class' => 'form-control'
				)
			);?>
		</div>
	</div>
	<div class="row form-group">
		<label for="address" class="col-sm-offset-2 col-sm-2 control-label text-right">Address:</label>
		<div class="col-sm-4">
			<?php echo form_input( 
				array(
					'name' => 'address',
					'value' => $this->session->userdata['address'],
					'class' => 'form-control'
				)
			);?>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-offset-4 col-sm-4">
			<input type="submit" name="submit" value="Submit" class="btn btn-info" />
		</div>
	</div>
	</form>
</div>
