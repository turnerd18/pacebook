<?php $this->load->view('includes/header') ?>

<?php if(isset($query_result)) : ?>
	<?php $this->load->view($content, $query_result); ?>
<?php else : ?>
	<?php $this->load->view($content); ?>
<?php endif; ?>

<?php $this->load->view('includes/footer') ?>
