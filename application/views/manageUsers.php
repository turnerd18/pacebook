<div class="row">
	<div class="col-md-offset-3 col-md-6 text-center">
		<h2>Manage Users</h2>
	</div>
</div>

<div class="row">
	<div class="col-md-offset-2 col-md-8">
		<table class="table table-striped">
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Networks</th>
				<th>Staff?</th>
			</tr>
			<?php foreach ($query_result as $row) : ?>
				<tr>
					<td><?php echo $row['name']; ?></td>
					<td><?php echo $row['email']; ?></td>
					<td><?php echo $row['networks']; ?></td>
					<td><?php echo $row['is_staff']==true; ?></td>
				</tr>
			<?php endforeach; ?>
		</table>
	</div>
</div>
