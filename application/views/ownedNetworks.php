<div class="row">
  <div class="col-md-offset-3 col-md-6 text-center">
    <h2> Pending Members </h2>
  </div>
</div>

<div class="row form-group">
  <div class="col-md-offset-3 col-md-6">
    <?php echo form_open('home/approveReject'); ?>
      <table class="table table-striped">
        <tr>
          <th>Network Name</th>
          <th>User</th>
          <th colspan=2> Approve or Reject </th>
        </tr>
        <?php for ($i = 0; $i < count($query_result); $i++) : ?>
          <?php if($query_result[$i]['network_name']!= '' && $query_result[$i]['status'] == 0) : ?>
            <tr>
              <td><?php echo $query_result[$i]['network_name']; ?></td>
              <td><?php echo $query_result[$i]['user_email']; ?></td>
              <td>
              <?php echo form_hidden("network_$i", $query_result[$i]['network_name']); ?>
              <?php echo form_hidden("email_$i", $query_result[$i]['user_email']); ?>
              <?php echo form_submit(
                array(
                  'name' => "approve_$i",
                  'value' => 'Approve',
                  'class' => 'btn btn-default'
                )
              ); ?> </td>
              <td><?php echo form_submit(
                array(
                  'name' => "reject_$i",
                  'value' => 'Reject',
                  'class' => 'btn btn-default'
                )
              ); ?></td>
            </tr>
          <?php endif; ?>
        <?php endfor; ?>
      </table>
    </form>
  </div>
</div>

<div class="row">
  <div class="col-md-offset-3 col-md-6 text-center">
    <h2> Members </h2>
  </div>
</div>

<div class="row form-group">
  <div class="col-md-offset-3 col-md-6">
      <table class="table table-striped">
        <tr>
          <th>Network Name</th>
          <th>User</th>
        </tr>
        <?php for ($i = 0; $i < count($query_result); $i++) : ?>
          <?php if($query_result[$i]['network_name'] != '' && $query_result[$i]['status'] == 1) : ?>
            <tr>
              <td><?php echo $query_result[$i]['network_name']; ?></td>
              <td><?php echo $query_result[$i]['user_email']; ?></td>
            </tr>
          <?php endif; ?>
        <?php endfor; ?>
      </table>
  </div>
</div>

<div class="row">
  <div class="col-md-offset-3 col-md-6 text-center">
    <h2> Rejected Members </h2>
  </div>
</div>

<div class="row form-group">
  <div class="col-md-offset-3 col-md-6">
      <table class="table table-striped">
        <tr>
          <th>Network Name</th>
          <th>User</th>
        </tr>
        <?php for ($i = 0; $i < count($query_result); $i++) : ?>
          <?php if($query_result[$i]['network_name']!= '' && $query_result[$i]['status'] == 2) : ?>
            <tr>
              <td><?php echo $query_result[$i]['network_name']; ?></td>
              <td><?php echo $query_result[$i]['user_email']; ?></td>
            </tr>
          <?php endif; ?>
        <?php endfor; ?>
      </table>
  </div>
</div>

<div class="row">
  <div class="col-md-offset-3 col-md-6 text-center">
    <h2> Accepted Networks </h2>
  </div>
</div>

<div class="row form-group">
  <div class="col-md-offset-3 col-md-6">
      <table class="table table-striped">
        <tr>
          <th>Network Name</th>
          <th></th>
        </tr>
        <?php for ($i = 0; $i < count($query_result); $i++) : ?>
          <?php if($query_result[$i]['name']!= '' && $query_result[$i]['status'] == 1) : ?>
            <tr>
              <td><?php echo $query_result[$i]['name']; ?></td>
              <td><?php ?></td>
            </tr>
          <?php endif; ?>
        <?php endfor; ?>
      </table>
  </div>
</div>

<div class="row">
  <div class="col-md-offset-3 col-md-6 text-center">
    <h2> Pending Networks </h2>
  </div>
</div>

<div class="row form-group">
  <div class="col-md-offset-3 col-md-6">
      <table class="table table-striped">
        <tr>
          <th>Network Name</th>
          <th></th>
        </tr>
        <?php for ($i = 0; $i < count($query_result); $i++) : ?>
          <?php if($query_result[$i]['name']!= '' && $query_result[$i]['status'] == 0) : ?>
            <tr>
              <td><?php echo $query_result[$i]['name']; ?></td>
              <td><?php ?></td>
            </tr>
          <?php endif; ?>
        <?php endfor; ?>
      </table>
  </div>
</div>

<div class="row">
  <div class="col-md-offset-3 col-md-6 text-center">
    <h2> Rejected Networks </h2>
  </div>
</div>

<div class="row form-group">
  <div class="col-md-offset-3 col-md-6">
      <table class="table table-striped">
        <tr>
          <th>Network Name</th>
          <th></th>
        </tr>
        <?php for ($i = 0; $i < count($query_result); $i++) : ?>
          <?php if($query_result[$i]['name']!= '' && $query_result[$i]['status'] == 2) : ?>
            <tr>
              <td><?php echo $query_result[$i]['name']; ?></td>
              <td><?php ?></td>
            </tr>
          <?php endif; ?>
        <?php endfor; ?>
      </table>
  </div>
</div>
