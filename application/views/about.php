<div class="container">
	<h1 class="text-center">Pacebook Technologies</h1>
	<div class="row">
        <div class="col-lg-4">
          <h2><img height="48" width="48" src="<?php echo base_url(); ?>assets/images/ci.png" /> CodeIgniter</h2>
          <p>CodeIgniter is an open source rapid development web application framework, for use in building dynamic web sites with PHP.</p>
          <p><a class="btn btn-default" href="http://ellislab.com/codeigniter/">CodeIgniter »</a></p>
        </div>
        <div class="col-lg-4">
          <h2><img height="48" width="48" src="<?php echo base_url(); ?>assets/images/bootstrap.png" /> Bootstrap</h2>
          <p>Bootstrap is a free collection of tools for creating websites and web applications. It contains HTML and CSS-based design templates for typography, forms, buttons, navigation and other interface components, as well as optional JavaScript extensions.</p>
          <p><a class="btn btn-default" href="http://getbootstrap.com/">Bootstrap »</a></p>
       </div>
        <div class="col-lg-4">
          <h2><img height="48" width="48" src="<?php echo base_url(); ?>assets/images/gravatar.jpg" /> Gravatar</h2>
          <p>All of our user photos are pulled in from Gravatar. Gravatar users can register an account based on their email address, and upload an avatar to be associated with the account.</p>
          <p><a class="btn btn-default" href="http://en.gravatar.com/">Gravatar »</a></p>
        </div>
      </div>
</div>