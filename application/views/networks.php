<div class="row">
	<div class="col-md-offset-3 col-md-6 text-center">
		<h2> My Networks </h2>
	</div>
</div>

<div class="row form-group">
	<div class="col-md-offset-3 col-md-6">
    <?php echo form_open('home/leaveNetwork'); ?>
      <table class="table table-striped">
        <tr>
          <th>Name</th>
          <th>Options</th>
        </tr>
        <?php for ($i = 0; $i < count($query_result); $i++) : ?>
          <?php if($query_result[$i]->status == 1) : ?>
          <tr>
            <td><?php echo $query_result[$i]->network_name; ?></td>
            <td>
              <?php echo form_hidden("network_$i", $query_result[$i]->network_name); ?>
              <?php echo form_submit( 
              array( 
                'name' => "submit_$i",
                'value' => 'Leave Network',
                'class' => 'btn btn-default'
              )	
            ); ?>
            </td>
          </tr>
          <?php endif; ?>
        <?php endfor; ?>
      </table>
    </form>
	</div>
</div>

<div class="row">
	<div class="col-md-offset-3 col-md-6 text-center">
		<h2> Pending Network Joins </h2>
	</div>
</div>

<div class="row form-group">
	<div class="col-md-offset-3 col-md-6">
    <table class="table table-striped">
        <tr>
          <th>Name</th>
        </tr>
        <?php for ($i = 0; $i < count($query_result); $i++) : ?>
          <?php if($query_result[$i]->status == 0) : ?>
          <tr>
            <td><?php echo $query_result[$i]->network_name; ?></td>
          </tr>
          <?php endif; ?>
        <?php endfor; ?>
      </table>
	</div>
</div>

<div class="row">
	<div class="col-md-offset-3 col-md-6 text-center">
		<h2> Rejected Network Joins </h2>
	</div>
</div>

<div class="row form-group">
	<div class="col-md-offset-3 col-md-6">
    <?php echo form_open('home/joinNetwork'); ?>
    <table class="table table-striped">
        <tr>
          <th>Name</th>
          <th>Options</th>
        </tr>
        <?php for ($i = 0; $i < count($query_result); $i++) : ?>
          <?php if($query_result[$i]->status == 2) : ?>
          <tr>
            <td><?php echo $query_result[$i]->network_name; ?></td>
            <td>
              <?php echo form_hidden("network_$i", $query_result[$i]->network_name); ?>
              <?php echo form_hidden("from", "networks"); ?>
              <?php echo form_submit( 
              array( 
                'name' => "submit_$i",
                'value' => 'Resend Application',
                'class' => 'btn btn-default'
              )	
            ); ?>
            </td>
          </tr>
          <?php endif; ?>
        <?php endfor; ?>
      </table>
    </form>
	</div>
</div>
