</div>
</div>
<div id="footer">
	<div class="container">
		<div class="text-muted pull-left">
			Pacebook 2013 &nbsp
			<a href="<?php echo base_url(); ?>index.php/home/about">About</a> &nbsp
			<a href="https://bitbucket.org/turnerd18/pacebook/src">Source</a>
		</div>
		<div class="text-muted pull-right">Devin Turner & Jon Harmon</div>
	</div>
</div>

<script src="//code.jquery.com/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
</body>
</html>
