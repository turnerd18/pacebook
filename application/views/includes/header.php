<!DOCTYPE html>
<html>
  <head>
    <title>Pacebook</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="<?php echo base_url(); ?>assets/css/pacebook.css" rel="stylesheet" media="screen">
	
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/favicon.png">

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../../assets/js/html5shiv.js"></script>
			<script src="../../assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

	<div id="wrap">
		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo base_url(); ?>">Pacebook</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="<?php echo base_url(); ?>" class="nav-glyph-home"><img src="<?php echo base_url(); ?>assets/glyphs/white/png/home_icon&24.png" /></a></li>
						<?php if(isset($this->session->userdata['logged_in'])) : ?>
						<li class="dropdown">
              <a href="#" class="dropdown-toggle nav-glyph" data-toggle="dropdown"><img src="<?php echo base_url(); ?>assets/glyphs/white/png/users_icon&24.png" /></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>index.php/home/friends">Friends</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/home/invitations">Friend Invites</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/home/list_members">List Members</a></li>
              </ul>
            </li>
						<li class="dropdown">
						<a href="#" class="dropdown-toggle nav-glyph-network" data-toggle="dropdown"><img src="<?php echo base_url(); ?>assets/glyphs/white/png/globe_3_icon&24.png" /></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>index.php/home/networks">My Networks</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/home/list_networks">List Networks</a><li>
                <li><a href="<?php echo base_url(); ?>index.php/home/owned_networks">Owned Networks</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/home/create_network">Create Network</a></li>
              </ul>
            </li>
					<?php endif; ?>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<?php if(isset($this->session->userdata['is_staff']) and $this->session->userdata['is_staff']) : ?>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Staff <b class="caret"></b></a>
								<ul class="dropdown-menu">
								<li><a href="<?php echo base_url(); ?>index.php/staffHome/manageUsers">Manage Users</a></li>
									<li><a href="<?php echo base_url(); ?>index.php/staffHome/manageNetworks">Manage Networks</a></li>
								</ul>
							</li>
					<?php endif; ?>
					<?php if(isset($this->session->userdata['logged_in'])) : ?>
						<li><img class="user-photo" src="<?php echo $this->session->userdata['photo'] . '&s=40'; ?>" /></li>
						<li><a href="<?php echo base_url(); ?>index.php/home/edit_profile"><?php echo $this->session->userdata['name']; ?></a></li>
						<li><a href="<?php echo base_url(); ?>index.php/login/logout">Log Out</a></li>
					<?php else : ?>
						<li><a href="<?php echo base_url(); ?>index.php/login/register_form">Register</a></li>
						<li><a href="<?php echo base_url(); ?>index.php/login">Login</a></li>
					<?php endif; ?>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
		<div class="container">
