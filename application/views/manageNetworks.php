<div class="row">
  <div class="col-md-offset-3 col-md-6 text-center">
    <h2> Pending Networks </h2>
  </div>
</div>

<div class="row form-group">
  <div class="col-md-offset-3 col-md-6">
    <?php echo form_open('staffHome/approveReject'); ?>
      <table class="table table-striped">
        <tr>
          <th>Network Name</th>
          <th>Owner</th>
          <th>Description</th>
          <th>Number of Members</th>
          <th colspan=2> Approve or Reject </th>
        </tr>
        <?php for ($i = 0; $i < count($query_result); $i++) : ?>
          <?php if($query_result[$i]['status'] == 0) : ?>
            <tr>
              <td><?php echo $query_result[$i]['name']; ?></td>
              <td><?php echo $query_result[$i]['owner']; ?></td>
              <td><?php echo $query_result[$i]['description']; ?></td>
              <td><?php echo $query_result[$i]['num_members']; ?></td>
              <td>
              <?php echo form_hidden("network_$i", $query_result[$i]['name']); ?>
              <?php echo form_submit(
                array(
                  'name' => "approve_$i",
                  'value' => 'Approve',
                  'class' => 'btn btn-default'
                )
              ); ?> </td>
              <td><?php echo form_submit(
                array(
                  'name' => "reject_$i",
                  'value' => 'Reject',
                  'class' => 'btn btn-default'
                )
              ); ?></td>
            </tr>
          <?php endif; ?>
        <?php endfor; ?>
      </table>
    </form>
  </div>
</div>

<div class="row">
  <div class="col-md-offset-3 col-md-6 text-center">
    <h2> Approved Networks </h2>
  </div>
</div>

<div class="col-md-offset-3 col-md-6">
  <table class="table table-striped">
    <tr>
      <th>Network Name</th>
      <th>Owner</th>
      <th>Description</th>
      <th>Number of Members</th>
    </tr>
    <?php foreach ($query_result as $row) : ?>
      <?php if($row['status'] == 1) :?>
        <tr>
          <td><?php echo $row['name']; ?></td>
          <td><?php echo $row['owner']; ?></td>
          <td><?php echo $row['description']; ?></td>
          <td><?php echo $row['num_members']; ?></td>
        </tr>
      <?php endif; ?>
    <?php endforeach; ?>
  </table>
</div>

<div class="row">
  <div class="col-md-offset-3 col-md-6 text-center">
    <h2> Rejected Networks </h2>
  </div>
</div>

<div class="col-md-offset-3 col-md-6">
  <table class="table table-striped">
    <tr>
      <th>Network Name</th>
      <th>Owner</th>
      <th>Description</th>
      <th>Number of Members</th>
    </tr>
    <?php foreach ($query_result as $row) : ?>
      <?php if($row['status'] == 2) :?>
        <tr>
          <td><?php echo $row['name']; ?></td>
          <td><?php echo $row['owner']; ?></td>
          <td><?php echo $row['description']; ?></td>
          <td><?php echo $row['num_members']; ?></td>
        </tr>
      <?php endif; ?>
    <?php endforeach; ?>
  </table>
</div>
