<div class="row">
	<div class="col-md-offset-3 col-md-6 text-center">
		<h2> Available Networks </h2>
	</div>
</div>

<div class="row form-group">
	<div class="col-md-offset-3 col-md-6">
    <?php echo form_open('home/joinNetwork'); ?>
      <table class="table table-striped">
        <tr>
          <th>Name</th>
          <th>Owner</th>
          <th>Number of Members</th>
          <th>Description</th>
          <th>Options</th>
        </tr>
        <?php for ($i = 0; $i < count($query_result); $i++) : ?>
          <tr>
            <td><?php echo $query_result[$i]['name']; ?></td>
            <td><?php echo $query_result[$i]['owner']; ?></td>
            <td><?php echo $query_result[$i]['num_members']; ?></td>
            <td><?php echo $query_result[$i]['description']; ?></td>
            <td>
              <?php echo form_hidden("network_$i", $query_result[$i]['name']); ?>
              <?php echo form_hidden("from", "list_networks"); ?>
              <?php echo form_submit( 
              array( 
                'name' => "submit_$i",
                'value' => 'Join Network',
                'class' => 'btn btn-default'
              )	
            ); ?>
            </td>
          </tr>
        <?php endfor; ?>
      </table>
    </form>
	</div>
</div>
