<h2 class="text-center">Setup a new account</h2>

<div style="padding: 20px 15px;">
	<?php echo form_open('login/register'); ?>
	<div class="row form-group">
		<label for="email" class="col-sm-offset-2 col-sm-2 control-label text-right">Email Address:</label>
		<div class="col-sm-4">
			<input type="text" name="email" value="name@example.com" class="form-control" />
		</div>
	</div>
	<div class="row form-group">
		<label for="password" class="col-sm-offset-2 col-sm-2 control-label text-right">Password:</label>
		<div class="col-sm-4">
			<input type="password" name="password" class="form-control" />
		</div>
	</div>
	<div class="row form-group">
		<label for="password2" class="col-sm-offset-2 col-sm-2 control-label text-right">Confirm Password:</label>
		<div class="col-sm-4">
			<input type="password" name="password2" class="form-control" />
		</div>
	</div>
	<div class="row form-group">
		<label for="name" class="col-sm-offset-2 col-sm-2 control-label text-right">Full Name:</label>
		<div class="col-sm-4">
			<input type="text" name="name" value="" class="form-control" />
		</div>
	</div>
	<div class="row form-group">
		<label for="phone" class="col-sm-offset-2 col-sm-2 control-label text-right">Phone Number:</label>
		<div class="col-sm-4">
			<input type="text" name="phone" value="555-555-5555" class="form-control" />
		</div>
	</div>
	<div class="row form-group">
		<label for="address" class="col-sm-offset-2 col-sm-2 control-label text-right">Address:</label>
		<div class="col-sm-4">
			<input type="text" name="address" value="" class="form-control" />
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-offset-4 col-sm-4">
			<input type="submit" name="submit" value="Submit" class="btn btn-info" />
		</div>
	</div>
</div>