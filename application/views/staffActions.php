<h2 class="starter-template">
<?php
	if(isset($this->session->userdata['logged_in']))
	{
		if($this->session->userdata['is_staff'])
		{
			echo "Staff actions under development.";
		} else 
		{
			redirect('home');
		}
	} else 
	{
		redirect('login');
	}
?>
</h2>