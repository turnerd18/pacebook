<div class="row">
  <div class="col-md-offset-3 col-md-6 text-center">
    <h2> My Friend Requests</h2>
  </div>
</div>

<div class="col-md-offset-3 col-md-6">
		<?php echo form_open('home/accept_reject_friend'); ?>
  <table class="table table-striped">
				<tr>
					<th></th>
					<th>Name</th>
					<th>Options</th>
				</tr>
				<?php for ($i = 0; $i < count($query_result); $i++) : ?>
					<?php if ($query_result[$i]['direction'] == 'received') : ?>
					<tr>
						<td><img src="<?php echo $query_result[$i]['photo'] . '&s=40'; ?>" /></td>
						<td><?php echo $query_result[$i]['name']; ?></td>
						<td>
							<?php echo form_hidden("friend_$i", $query_result[$i]['email']); ?>
							<?php echo form_submit( 
								array( 
									'name' => "approve_$i", 
									'value' => 'Accept', 
									'class' => 'btn-sm btn-default'
								) 
							); ?>
							<?php echo form_submit( 
								array( 
									'name' => "reject_$i", 
									'value' => 'Reject', 
									'class' => 'btn-sm btn-default'
								) 
							); ?>
						</td>
					</tr>
					<?php endif; ?>
				<?php endfor; ?>
	</table>
</form>
</div>

<div class="row">
  <div class="col-md-offset-3 col-md-6 text-center">
    <h2> Sent Friend Requests</h2>
  </div>
</div>

<div class="row form-group">
  <div class="col-md-offset-3 col-md-6">
      <table class="table table-striped">
        <tr>
          <th></th>
          <th>Name</th>
				</tr>
				<?php for ($i = 0; $i < count($query_result); $i++) : ?>
					<?php if ($query_result[$i]['direction'] == 'sent') : ?>
						<tr>
							<td><img src="<?php echo $query_result[$i]['photo'] . '&s=40'; ?>" /></td>
							<td><?php echo $query_result[$i]['name']; ?></td>
						</tr>
					<?php endif; ?>
        <?php endfor; ?>
      </table>
  </div>
</div>
