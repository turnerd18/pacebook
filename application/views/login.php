<h2 class="text-center">User Login</h2>

<div style="padding: 20px 15px;">
	<?php
		echo form_open('login/validate_credentials');
		echo "<div class=\"row form-group\">";
			echo form_label('Email address:', 'username', array('class' => 'col-sm-offset-2 col-sm-2 control-label text-right'));
			echo "<div class=\"col-sm-4\">";
				echo form_input(	
					array(
						'name' => 'username', 
						'class' => 'form-control', 
						'value' => set_value('username', 'user@example.com')
					)
				);
			echo "</div>"; // col-sm-4
		echo "</div>"; // row
		
		echo "<div class=\"row form-group\">";
			echo form_label('Password:', 'password', array('class' => 'col-sm-offset-2 col-sm-2 control-label text-right'));
			echo "<div class=\"col-sm-4\">";
				echo form_password(
					array(
						'name' => 'password', 
						'class' => 'form-control'
					)
				);
			echo "</div>"; // col-sm-4
		echo "</div>"; // row
		
		echo "<div class=\"row form-group\">";
			echo "<div class=\"col-sm-offset-4 col-sm-4 text-center\">";
				echo form_submit(
					array(
						'name' => 'submit',
						'value' => 'Log In',
						'class' => 'btn btn-info'
					)
				);
				echo anchor('login/register_form', 'Register', array('class' => 'btn btn-info', 'style' => 'margin-left: 10px;'));
			echo "</div>"; // col-sm-4
		echo "</div>"; // row
	?>
</div>
