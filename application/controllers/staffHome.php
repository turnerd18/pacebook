<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class StaffHome extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		// makes user login or register before they can visit other pages
		if (!isset($this->session->userdata['logged_in']))
		{
			redirect('login');
		}
		if(!$this->session->userdata['is_staff'])
		{
			redirect('home');
		}
	}
	function staffActions()
	{
		$data['content'] = 'staffActions';
		$this->load->view('template', $data);
	}
	
	function index()
	{
		$data['content'] = 'staffHome';
		$this->load->view('template', $data);
	}

	function manageUsers()
	{
		$users = $this->db->query('SELECT * FROM pb_Users');
		$networks = $this->db->query('SELECT * FROM pb_Membership');
		$result = array();

		foreach ($users->result() as $user)
		{
			array_push($result,
									array(
										'name' => $user->name,
										'email' => $user->email,
										'is_staff' => $user->is_staff,
										'networks' => ''
									)
								);
		}

		for ($i = 0; $i < count($result); $i++)
		{
			foreach ($networks->result() as $network)
			{
				// Check if user is an accepted member of the network
				if (($network->user_email == $result[$i]['email']) && $network->status == 1)
				{
					$result[$i]['networks'] .= $network->network_name . ', ';
				}
			}
			$result[$i]['networks'] = substr($result[$i]['networks'], 0, -2);
		}

		$data['query_result'] = $result;
		$data['content'] = 'manageUsers';
		$this->load->view('template', $data);
	}

	function manageNetworks()
	{
		$networks = $this->db->query('SELECT * FROM pb_Networks ORDER BY status');
		$result = array();

		foreach ($networks->result() as $network)
		{
			array_push($result,
									array(
										'name' => $network->name,
										'owner' => $network->owner,
										'description' => $network->description,
										'status' => $network->status,
										'num_members' => ''
									)
								);
		}
		
		for ($i = 0; $i < count($result); $i++)
		{
			$name = $result[$i]['name'];
			/*
			$count = $this->db->query("SELECT COUNT(network_name) FROM pb_Membership WHERE network_name=\"$name\" AND status=\"1\"");
			log_message('debug', $count->row()['COUNT(network_name)']);
			$result[$i]['num_members'] = $count->row()->network_name;
			*/
			$count = $this->db->query("SELECT * FROM pb_Membership WHERE network_name=\"$name\" AND status=\"1\"");
			$result[$i]['num_members'] = count($count->result());
		}

		$data['query_result'] = $result;
		$data['content'] = 'manageNetworks';
		$this->load->view('template', $data);
	}
  
  function approveReject()
  {
    $post_data = $this->input->post();
    $i = 0;
    $approveReject = 0;
    for (; $i < count($post_data); $i++)
    {
      if (isset($post_data['approve_' . $i]))
      {
        $approveReject = 1;
        break;
      }
      if (isset($post_data['reject_' . $i]))
      {
        $approveReject = 2;
        break;
      }
    }
    $n_name = $post_data['network_' . $i];
    $this->db->simple_query("UPDATE pb_Networks 
                            SET status=\"$approveReject\" 
                            WHERE name=\"$n_name\"");
    if ($approveReject == 1)
    {
      $query = $this->db->query("SELECT name, owner FROM pb_Networks
                                WHERE name=\"$n_name\"");
      $u_email = $query->result()[0]->owner;
      $this->db->simple_query("INSERT INTO pb_Membership (user_email,
                              network_name, status) VALUES (\"$u_email\", \"$n_name\", 1)");
    }
    redirect('staffHome/manageNetworks');
  }
}
