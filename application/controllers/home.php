<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// makes user login or register before they can visit other pages
		if (!isset($this->session->userdata['logged_in']))
		{
			redirect('login');
		}
	}

	function index()
	{
		$data['content'] = 'home';
		$this->load->view('template', $data);
	}

	function about()
	{
		$data['content'] = 'about';
		$this->load->view('template', $data);
	}

	function list_members()
	{
		$data['content'] = 'listMembers';
		$email = $this->session->userdata['email'];
		$query = $this->db->query("
			SELECT name,email,photo FROM pb_Users WHERE email!='$email' AND email NOT IN 
			(
				SELECT sender_email
				FROM pb_Friends 
				WHERE receiver_email='$email' AND status !=2
				
				UNION
				
				SELECT receiver_email 
				FROM pb_Friends 
				WHERE sender_email='$email' AND status !=2
			);");
		$data['query_result'] = $query->result();
		$this->load->view('template', $data);
	}

  function list_networks()
  {
   $email = $this->session->userdata['email'];
   $query = $this->db->query("
    SELECT name, owner, description FROM pb_Networks WHERE status = '1' AND (name) NOT IN
      (
        SELECT network_name
        FROM pb_Membership
        WHERE user_email = '$email' AND status != '2'
      );");

   $result = array();
   foreach ($query->result() as $network)
   {
     array_push($result,
                array(
                  'name' => $network->name,
                  'owner' => $network->owner,
                  'num_members' => '',
                  'description' => $network->description
                )
              );
   }

   for ($i = 0; $i < count($result); $i++)
   {
     $name = $result[$i]['name'];
     $count = $this->db->query("SELECT * FROM pb_Membership WHERE network_name=\"$name\" AND status=\"1\"");
     $result[$i]['num_members'] = count($count->result());
   }

   $data['query_result'] = $result;
   $data['content'] = 'listNetworks';
   $this->load->view('template', $data);
  }

	function friends()
	{
		$data['content'] = 'friends';
		$email = $this->session->userdata['email'];
		$query = $this->db->query("
			SELECT name,email,photo FROM pb_Users WHERE email!='$email' AND email IN 
			(
				SELECT sender_email
				FROM pb_Friends 
				WHERE receiver_email='$email' AND status=1
				
				UNION
				
				SELECT receiver_email 
				FROM pb_Friends 
				WHERE sender_email='$email' AND status=1
			);");
		$data['query_result'] = $query->result();
		$this->load->view('template', $data);
	}

	function invitations()
	{
		$data['content'] = 'invitations';
		$email = $this->session->userdata['email'];
		$result = array();
		// sent invites
		$query = $this->db->query("
			SELECT name,email,photo FROM pb_Users WHERE email!='$email' AND email IN 
			(
				SELECT receiver_email
				FROM pb_Friends 
				WHERE sender_email='$email' AND status=0
			);");

		foreach ($query->result() as $sent_to)
		{
			array_push($result,
				array(
					'name' => $sent_to->name,
					'email' => $sent_to->email,
					'photo' => $sent_to->photo,
					'direction' => 'sent'
				)
			);
		}

		$query = $this->db->query("
			SELECT name,email,photo FROM pb_Users WHERE email!='$email' AND email IN 
			(
				SELECT sender_email
				FROM pb_Friends 
				WHERE receiver_email='$email' AND status=0
			);");

		foreach ($query->result() as $received)
		{
			array_push($result,
				array(
					'name' => $received->name,
					'email' => $received->email,
					'photo' => $received->photo,
					'direction' => 'received'
				)
			);
		}

		$data['query_result'] = $result;
		$this->load->view('template', $data);

	}

	function accept_reject_friend()
	{
		$post_data = $this->input->post();
		$i = 0;
    $accept_reject = 0;
    for (; $i < count($post_data); $i++)
    {
      if (isset($post_data['approve_' . $i]))
      {
        $accept_reject = 1;
        break;
      }
      if (isset($post_data['reject_' . $i]))
      {
        $accept_reject = 2;
        break;
      }
    }
		$u_email = $this->session->userdata['email'];
		$f_email = $post_data['friend_' . $i];
		$this->db->simple_query("UPDATE pb_Friends 
															SET status='$accept_reject'
															WHERE sender_email='$f_email' 
															AND receiver_email='$u_email';");
		redirect('home/invitations');
	}

	function addFriend()
	{
		$post_data = $this->input->post();
		$i = 0;
		for (; $i < count($post_data); $i++)
		{
			if (isset($post_data['submit_' . $i]))
			{
				break;
			}
		}
		$u_email = $this->session->userdata['email'];
		$f_email = $post_data['friend_' . $i];
		$query = $this->db->query("
			SELECT sender_email 
			FROM pb_Friends
			WHERE (sender_email='$u_email' AND receiver_email='$f_email') 
			OR (sender_email='$f_email' AND receiver_email='$u_email');
		");

		if ($query->num_rows() > 0) 
		{
			if ($query->result()[0]->sender_email == $u_email)
			{
				$this->db->simple_query("
					UPDATE pb_Friends 
					SET status=0 
					WHERE sender_email='$u_email' 
					AND receiver_email='$f_email';
				");
			}
			else
			{
				$this->db->simple_query("
					DELETE FROM pb_Friends 
					WHERE sender_email='$f_email' 
					AND receiver_email='$u_email';
				");
				$this->db->simple_query("
					INSERT INTO pb_Friends (sender_email, receiver_email, status)
					VALUES ('$u_email', '$f_email', 0);
				");
			}
		}
		else
		{
			$this->db->simple_query("
				INSERT INTO pb_Friends (sender_email, receiver_email, status)
				VALUES ('$u_email', '$f_email', 0);
			");
		}
					
		redirect('home/list_members');
	}

	function removeFriend()
	{
		$post_data = $this->input->post();
		$i = 0;
		for (; $i < count($post_data); $i++)
		{
			if (isset($post_data['submit_' . $i]))
			{
				break;
			}
		}
		$u_email = $this->session->userdata['email'];
		$f_email = $post_data['friend_' . $i];
		$this->db->simple_query("DELETE FROM pb_Friends WHERE
														(sender_email='$f_email' AND receiver_email='$u_email') OR
														(sender_email='$u_email' AND receiver_email='$f_email')"
													);
		redirect('home/friends');
	}

  function networks()
  {
    $data['content'] = 'networks';
    $email = $this->session->userdata['email'];
    $query = $this->db->query("
      SELECT network_name, status
      FROM pb_Membership
      WHERE user_email = '$email'
      ");
    $data['query_result'] = $query->result();
    $this->load->view('template', $data);
	}

	function edit_profile()
	{
		$data['content'] = 'edit_profile';
		$this->load->view('template', $data);
	}

	function update_profile()
	{
		$this->load->model('user');
		$result = $this->user->update();
		if ($result == '')
		{
			redirect('home/edit_profile');
		}
		else
		{
			$this->session->userdata['name'] = $this->input->post('name');
			$this->session->userdata['email'] = $this->input->post('email');
			$this->session->userdata['address'] = $this->input->post( 'address' );
			$this->session->userdata['phone'] = $this->input->post( 'phone' );
			$this->session->userdata['photo'] = 'http://www.gravatar.com/avatar/' . md5(strtolower(trim($this->input->post( 'email' )))) . '.jpg' . '?d=' . urlencode( base_url() . 'assets/images/nopic.jpg');
			redirect('home/index');
		}
	}

  function leaveNetwork()
  {
		$post_data = $this->input->post();
		$i = 0;
		for (; $i < count($post_data); $i++)
		{
			if (isset($post_data['submit_' . $i]))
			{
				break;
			}
		}
		$u_email = $this->session->userdata['email'];
		$n_name = $post_data['network_' . $i];
		$this->db->simple_query("DELETE FROM pb_Membership WHERE
														user_email=\"$u_email\" AND network_name=\"$n_name\";"
													);
		redirect('home/networks');
  }

  function joinNetwork()
  {
    $post_data = $this->input->post();
    $i = 0;
    for (; $i < count($post_data); $i++)
    {
      if (isset($post_data['submit_' . $i]))
      {
        break;
      }
    }
    $u_email = $this->session->userdata['email'];
    $n_name = $post_data['network_' . $i];
    $this->db->query("INSERT INTO pb_Membership (user_email, network_name)
                      VALUES('$u_email', '$n_name') ON
                      DUPLICATE KEY UPDATE status=0");

    redirect('home/' . $post_data['from']);
  }

  function owned_networks()
  {
    $results = array();
    $email = $this->session->userdata['email'];
    $query = $this->db->query("SELECT user_email, network_name, pb_Membership.status
                              FROM pb_Membership, pb_Networks
                              WHERE network_name = pb_Networks.name AND
                              pb_Networks.owner = '$email'");
    foreach ($query->result() as $res)
    {
      array_push($results,
                  array(
                  'user_email' => $res->user_email,
                  'network_name' => $res->network_name,
                  'status' => $res->status,
                  'name' => ''));
    }
    $query = $this->db->query("SELECT name, status FROM pb_Networks
                              WHERE owner=\"$email\"");
    foreach ($query->result() as $res)
    {
      array_push($results,
                  array(
                  'user_email' => '',
                  'network_name' => '',
                  'status' => $res->status,
                  'name' => $res->name));
    }           
    $data['content'] = "ownedNetworks";
    $data['query_result'] = $results;
    $this->load->view('template', $data);
  }

  function approveReject()
  {
    $post_data = $this->input->post();
    $i = 0;
    $approveReject = 0;
    for (; $i < count($post_data); $i++)
    {
      if (isset($post_data['approve_' . $i]))
      {
        $approveReject = 1;
        break;
      }
      if (isset($post_data['reject_' . $i]))
      {
        $approveReject = 2;
        break;
      }
    }
    $n_name = $post_data['network_' . $i];
    $u_email = $post_data['email_' . $i];
    $this->db->simple_query("UPDATE pb_Membership 
                            SET status=\"$approveReject\" 
                            WHERE network_name=\"$n_name\" AND
                            user_email=\"$u_email\";");
    redirect('home/owned_networks');
  }

  function create_network()
  {
    $data['content'] = 'createNetwork';
    $this->load->view('template', $data);
  }

  function createNetwork()
  {
    $post_data = $this->input->post();
    $name = $post_data['name'];
    $owner = $this->session->userdata['email'];
    $description = $post_data['description'];
    log_message('debug',"name: " . $name);
    $query = $this->db->query("SELECT name FROM pb_Networks WHERE name=\"$name\" AND status != 2");
    if($query->num_rows() == 0)
    {
      $this->db->query("INSERT INTO pb_Networks (name, owner, description)
                        VALUES (\"$name\",\"$owner\", \"$description\")
                        ON DUPLICATE KEY UPDATE status=0, description=\"$description\"
                        ,owner=\"$owner\"");
      redirect('home/owned_networks');
    }
    else
    {
      redirect('home/create_network');
    }
  }

}
