<?php

class Login extends CI_Controller
{
	function index()
	{
		$data['content'] = 'login';
		$this->load->view('template', $data);
	}
	
	function validate_credentials()
	{
		$this->load->model('user');
		$data = $this->user->validate();

		// Check if valid
		if(isset($data['name']))
		{
			$data['logged_in'] = true;
			$this->session->set_userdata($data);
      if($data['is_staff'])
      {
        redirect('staffHome');
      } else 
      {
  			redirect('home');
      }
		}
		else
		{
			$this->index();
		}
	}
	
	function register_form()
	{
		$data['content'] = 'register';
		$this->load->view('template', $data);
	}
	
	function register()
	{
		$this->load->model('user');
		$data = $this->user->register();
		
		// Check if valid
		if(isset($data['name']))
		{
			$data['logged_in'] = true;
			$this->session->set_userdata($data);
			redirect('home');
		}
		else
		{
			$this->register_form();
		}
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		$this->index();
	}
}
